import re
from enum import IntEnum


input_filename = 'input.txt'
# input_filename = 'test.txt'
# input_filename = 'test2.txt'

in_re = re.compile(r'<x= *([^,]+), y= *([^,]+), z= *([^,]+)>')

class Vector3D(object):
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return f'<x: {self.x:>4}, y: {self.y:>4}, z: {self.z:>4}>'

    def __str__(self):
        return self.__repr__()

    def add(self, vec):
        self.x += vec.x
        self.y += vec.y
        self.z += vec.z

    def get_energy(self):
        return abs(self.x) + abs(self.y) + abs(self.z)


lines = []
with open(input_filename, 'r') as f:
   lines = f.read().strip(' \n').split('\n')


moons = []
for l in lines:
    result = in_re.match(l)
    groups = result.groups()

    coords = []
    for g in groups:
        coords.append(int(g))

    pos = Vector3D(coords[0], coords[1], coords[2])
    vel = Vector3D(0, 0, 0)
    moons.append({'pos': pos, 'vel': vel})


def total_energy():
    e = 0
    for m in moons:
        e += m['pos'].get_energy() * m['vel'].get_energy()
    return e


def print_moons(steps):
    print('After ' + str(steps) + ' steps:')
    for m in moons:
        print(f'pos={m["pos"]}, vel={m["vel"]}')
    print()


steps = 0
print_moons(steps)

while steps < 1000:
    steps += 1

    # calc gravity
    for i, m in enumerate(moons):
        gravity = Vector3D(0, 0, 0)

        for j, mm in enumerate(moons):
            pos = mm['pos']

            if i == j:
                continue

            if pos.x > m['pos'].x:
                gravity.x += 1
            elif pos.x < m['pos'].x:
                gravity.x -= 1

            if pos.y > m['pos'].y:
                gravity.y += 1
            elif pos.y < m['pos'].y:
                gravity.y -= 1

            if pos.z > m['pos'].z:
                gravity.z += 1
            elif pos.z < m['pos'].z:
                gravity.z -= 1

        m['vel'].add(gravity)

    for m in moons:
        m['pos'].add(m['vel'])

    print_moons(steps)

print(total_energy())
