from int_comp import IntComputer, IOQueueFunctor
import queue
import threading
from enum import IntEnum


input_filename = 'input.txt'

rom = []
with open(input_filename, 'r') as f:
   rom = [int(s) for s in f.read().strip(' \n').split(',')]


camera = queue.SimpleQueue()
actor = queue.SimpleQueue()
stopped = False

def comp_thread():
    global stopped
    comp = IntComputer(rom, IOQueueFunctor(camera, actor))
    comp.start()

    actor.put(None)
    actor.put(None)
    stopped = True

t = threading.Thread(target=comp_thread)
t.start()

class Color(IntEnum):
    BLACK = 0
    WHITE = 1

    def to_char(self):
        if self == self.BLACK:
            return ' '
        if self == self.WHITE:
            return '#'


coords = (0, 0)
map = {}

def get_color(x, y):
    if not x in map:
        map[x] = {}
    if not y in map[x]:
        map[x][y] = Color.BLACK
    return map[x][y]

def set_color(x, y, color):
    if not x in map:
        map[x] = {}
    if not y in map[x]:
        map[x][y] = Color.BLACK
    map[x][y] = Color(color)

set_color(0, 0, Color.WHITE)


class Direction(IntEnum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3

    def rotate(self, dir):
        i = int(self) + 4
        if dir == 0:
            i -= 1
        else:
            i += 1
        i %= 4
        return Direction(i)

    def advance(self, coords):
        if self == self.UP:
            return (coords[0], coords[1]-1)
        elif self == self.RIGHT:
            return (coords[0]+1, coords[1])
        elif self == self.DOWN:
            return (coords[0], coords[1]+1)
        elif self == self.LEFT:
            return (coords[0]-1, coords[1])
        raise KeyError('Unknown direction found ' + str(int(self)))

    def to_char(self):
        if self == self.UP:
            return '^'
        elif self == self.RIGHT:
            return '>'
        elif self == self.DOWN:
            return 'v'
        elif self == self.LEFT:
            return '<'
        raise KeyError('Unknown direction found ' + str(int(self)))



dir = Direction.UP

def print_map(map, minx, miny, maxx, maxy):
    for y in range(miny, maxy+1):
        for x in range(minx, maxx+1):
            if coords[0] == x and coords[1] == y:
                print(dir.to_char(), end='')
            elif x not in map or y not in map[x]:
                print(Color.BLACK.to_char(), end='')
            else:
                print(map[x][y].to_char(), end='')

        print()

minx = 0
maxx = 0
miny = 0
maxy = 0

while not stopped:
    curr_color = get_color(coords[0], coords[1])
    camera.put(curr_color)

    next_color = actor.get()
    rotation = actor.get()

    if next_color is None or rotation is None:
        print('Computer already ended')
        continue

    dir = dir.rotate(rotation)

    #print(f'Color: {next_color}, Rotation: {rotation}')

    set_color(coords[0], coords[1], Color(next_color))
    coords = dir.advance(coords)

    if coords[0] < minx:
        minx = coords[0]
    if coords[0] > maxx:
        maxx = coords[0]
    if coords[1] < miny:
        miny = coords[1]
    if coords[1] > maxy:
        maxy = coords[1]

    print(f'x: {coords[0]}, y: {coords[1]}')

print_map(map, minx, miny, maxx, maxy)

count = 0
for k in map:
    count += len(map[k])

print(count)
