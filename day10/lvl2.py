import copy

input_filename = 'input.txt'
station = (23, 20)
# input_filename = 'test5.txt'
# station = (11, 13)
# input_filename = 'test6.txt'
# station = (8, 3)
# input_filename = 'test7.txt'
# station = (2, 2)

map = []
with open(input_filename, 'r') as f:
    for l in f.read().strip(' \n').split('\n'):
        map.append([c for c in l])

h = len(map)
w = len(map[0])




def sign(i):
    if i == 0:
        return 0
    elif i > 0:
        return 1
    else:
        return -1

class Angle(object):
    def __init__(self, coords):
        self.dx = None
        self.dy = None
        self.origin = None
        self.coords = coords

    def set_origin(self, origin):
        self.dx = self.coords[0] - origin[0]
        self.dy = self.coords[1] - origin[1]
        self.origin = origin

    def __str__(self):
        return f'{self.coords}'
    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        if self.origin is None or other.origin is None:
            return False

        # check all cases where the two points align horizontally with the origin
        if sign(self.dx) == 0 and sign(self.dx) == sign(other.dx):
            return sign(self.dy) != sign(other.dy) and sign(self.dy) == -1
        elif sign(self.dx) == 0 and sign(self.dy) == -1:
            return True
        elif sign(self.dx) == 0 and sign(self.dy) == 1:
            return sign(other.dx) == -1
        elif sign(other.dx) == 0 and sign(other.dy) == -1:
            return False
        elif sign(other.dx) == 0 and sign(other.dy) == 1:
            return sign(self.dx) == 1

        # check cases where points lay in different half planes
        if sign(self.dx) > sign(other.dx):
            return True
        elif sign(self.dx) < sign(other.dx):
            return False

        # calculte gradients for both points through origin
        k = -1 * self.dy / self.dx
        ok = -1 * other.dy / other.dx
        return k > ok



asteroids = []
for y, row in enumerate(map):
    for x, cell in enumerate(row):
        if cell == '#':
            angle = Angle((x, y))
            angle.set_origin(station)
            asteroids.append(angle)

found = False
for a in asteroids[:]:
    if station == a.coords:
        found = True
        asteroids.remove(a)
        break
if not found:
    print('Station is no valid asteroid')
    exit()

map[station[1]][station[0]] = 'X'


asteroids.sort()


def asteroid_in_sight(asteroid):
    tolerance = 0.01
    ox, oy = asteroid.origin

    # calculate deltas
    dx, dy = asteroid.coords
    dx -= ox
    dy -= oy

    # determine dominant dimension
    domx = abs(dx) > abs(dy)

    if domx:
        deltas = (dx, dy)
        coords = (ox, oy)
        coords_max = (w, h)
    else:
        deltas = (dy, dx)
        coords = (oy, ox)
        coords_max = (h, w)

    # check for other asteroids blocking line of sight
    k = abs(1.0 * deltas[1] / deltas[0])
    for step in range(1, abs(deltas[0])):
        c1 = coords[1] + step*k*sign(deltas[1])
        c0 = coords[0] + step*sign(deltas[0])

        if c0 >= coords_max[0] or c1 >= coords_max[1] or c0 < 0 or c1 < 0:
            continue

        # distance from asteroid
        d = c1 - int(c1)

        cell = '!'
        if domx:
            cell = map[int(c1)][c0]
        else:
            cell = map[c0][int(c1)]

        if d < tolerance and cell == '#':
            return False

    return True

count = 0
def do_rotation():
    global count
    global map
    global asteroids

    new_asteroids = []
    new_map = copy.deepcopy(map)

    for a in asteroids:
        if asteroid_in_sight(a):
            count += 1
            if count == 200:
                print(f'{count}: {a}')
            new_map[a.coords[1]][a.coords[0]] = 'x'
            # print()
            # for l in new_map:
            #     print(''.join(l))
            # new_map[a.coords[1]][a.coords[0]] = '.'
            # input()
        else:
            new_asteroids.append(a)

    map = new_map
    asteroids = new_asteroids


while len(asteroids) > 0:
    do_rotation()
