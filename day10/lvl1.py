input_filename = 'input.txt'
# input_filename = 'test5.txt'

map = []
with open(input_filename, 'r') as f:
    for l in f.read().strip(' \n').split('\n'):
        map.append([c for c in l])

h = len(map)
w = len(map[0])

asteroids = {}
for y, row in enumerate(map):
    for x, cell in enumerate(row):
        if cell == '#':
            asteroids[(x, y)] = None

def sign(i):
    if i == 0:
        return 0
    elif i > 0:
        return 1
    else:
        return -1

def asteroids_in_sight(origin):
    tolerance = 0.01
    ox, oy = origin

    in_sight = 0
    for a in asteroids:
        if a is origin:
            continue
        blocked = False

        # calculate deltas
        dx, dy = a
        dx -= ox
        dy -= oy

        # determine dominant dimension
        domx = abs(dx) > abs(dy)

        x = ox
        y = oy

        if domx:
            deltas = (dx, dy)
            coords = (x, y)
            coords_max = (w, h)
        else:
            deltas = (dy, dx)
            coords = (y, x)
            coords_max = (h, w)

        # check for other asteroids blocking line of sight
        k = abs(1.0 * deltas[1] / deltas[0])
        for step in range(1, abs(deltas[0])):
            c1 = coords[1] + step*k*sign(deltas[1])
            c0 = coords[0] + step*sign(deltas[0])

            if c0 >= coords_max[0] or c1 >= coords_max[1] or c0 < 0 or c1 < 0:
                continue

            # distance
            d = c1 - int(c1)

            cell = '!'
            if domx:
                cell = map[int(c1)][c0]
            else:
                cell = map[c0][int(c1)]

            if d < tolerance and cell == '#':
                blocked = True
                break

        if not blocked:
            in_sight += 1

    return in_sight


# for l in map:
#     print(''.join(l))

for a in asteroids:
    asteroids[a] = asteroids_in_sight(a)

# for a in asteroids:
#     print(f'{a}: {asteroids[a]}')


ma = max(asteroids, key=lambda a: asteroids[a])
print(ma)
print(asteroids[ma])
