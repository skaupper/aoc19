import copy

input_filename = 'input.txt'
# input_filename = 'test4.txt'
output_filename = 'output.txt'

rom = []
with open(input_filename, 'r') as f:
   rom = [int(s) for s in f.read().strip(' \n').split(',')]


def getParam(ram, ip, idx, modes):
    val = ram[ip+idx+1]

    if len(modes) <= idx:
        mode = 0
    else:
        mode = modes[idx]

    if mode == 0:   # position mode
        return ram[val]
    elif mode == 1: # immediate mode
        return val
    raise KeyError("Unknown parameter mode detected")


ram = []

# initialize RAM
ram = copy.deepcopy(rom)

ip = 0
while True:
    op = int(str(ram[ip])[-2:])
    modes = [int(s) for s in reversed(str(ram[ip])[:-2])]
    # print(f'op: {op}; mode: {modes}')

    if op == 1:
        p1 = getParam(ram, ip, 0, modes)
        p2 = getParam(ram, ip, 1, modes)
        dest = ram[ip+3]
        ram[dest] = p1+p2
        ip += 4
    elif op == 2:
        p1 = getParam(ram, ip, 0, modes)
        p2 = getParam(ram, ip, 1, modes)
        dest = ram[ip+3]
        ram[dest] = p1*p2
        ip += 4
    elif op == 3:
        dest = ram[ip+1]
        val = int(input())
        ram[dest] = val
        ip += 2
    elif op == 4:
        src = getParam(ram, ip, 0, modes)
        print(src)
        ip += 2
    elif op == 5:
        p1 = getParam(ram, ip, 0, modes)
        p2 = getParam(ram, ip, 1, modes)
        if p1 != 0:
            ip = p2
        else:
            ip += 3
    elif op == 6:
        p1 = getParam(ram, ip, 0, modes)
        p2 = getParam(ram, ip, 1, modes)
        if p1 == 0:
            ip = p2
        else:
            ip += 3
    elif op == 7:
        p1 = getParam(ram, ip, 0, modes)
        p2 = getParam(ram, ip, 1, modes)
        dest = ram[ip+3]
        if p1 < p2:
            ram[dest] = 1
        else:
            ram[dest] = 0
        ip += 4
    elif op == 8:
        p1 = getParam(ram, ip, 0, modes)
        p2 = getParam(ram, ip, 1, modes)
        dest = ram[ip+3]
        if p1 == p2:
            ram[dest] = 1
        else:
            ram[dest] = 0
        ip += 4
    elif op == 99:
        break
    else:
        raise KeyError('Unknown operation detected')


# for c in ram:
#     print(f'{c} ', end='')
