from int_comp import IntComputer

input_filename = 'input.txt'

rom = []
with open(input_filename, 'r') as f:
   rom = [int(s) for s in f.read().strip(' \n').split(',')]

comp = IntComputer(rom)
comp.set_ram_size(4096)
comp.start()
