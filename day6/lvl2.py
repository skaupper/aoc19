import copy

input_filename = 'input.txt'
# input_filename = 'test2.txt'
output_filename = 'output.txt'


class SpaceObject:
    def __init__(self, name):
        self.name = name
        self.orbits = []
        self.parent = None

    def add_child(self, orbiting_name):
        self.orbits.append(orbiting_name)

    def set_parent(self, name):
        self.parent = name

    def get_children(self):
        return self.orbits

    def get_parent(self):
        return self.parent


space_objects = {}

lines = []
with open(input_filename, 'r') as f:
   lines = f.read().strip('\n').split('\n')
for l in lines:
    obj, orbiting_object = l.split(')')

    if obj not in space_objects:
        space_objects[obj] = SpaceObject(obj)
    if orbiting_object not in space_objects:
        space_objects[orbiting_object] = SpaceObject(orbiting_object)

    space_objects[obj].add_child(orbiting_object)
    space_objects[orbiting_object].set_parent(obj)

# for obj in space_objects:
#     print(f'{obj}: ', end='')
#     for orbiting in space_objects[obj].get_children():
#         print(f'{orbiting} ', end='')
#     print()

# for obj in space_objects:
#     print(f'{obj}: ', end='')
#     # for orbiting in space_objects[obj].get_children():
#     print(f'{space_objects[obj].get_parent()} ', end='')
#     print()

def steps_to_santa(space_objects, obj, coming_from):
    possibilities = space_objects[obj].get_children()[:] + [space_objects[obj].get_parent()]
    try:
        possibilities.remove(coming_from)
    except:
        pass

    if len(possibilities) == 0:
        return -1
    if 'SAN' in possibilities:
        return 0

    first = True
    mind = 0
    for p in possibilities:
        if p is None:
            continue
        d = steps_to_santa(space_objects, p, obj)
        if d == -1:
            continue
        if first or d+1 < mind:
            first = False
            mind = d+1
    if first:
        return -1
    return mind

print(steps_to_santa(space_objects, 'YOU', None)-1)
