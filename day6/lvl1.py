import copy

input_filename = 'input.txt'
# input_filename = 'test.txt'
output_filename = 'output.txt'


class SpaceObject:
    def __init__(self, name):
        self.name = name
        self.orbits = []

    def add_orbit(self, orbiting_name):
        self.orbits.append(orbiting_name)

    def get_orbits(self):
        return self.orbits

space_objects = {}

lines = []
with open(input_filename, 'r') as f:
   lines = f.read().strip('\n').split('\n')
for l in lines:
    obj, orbiting_object = l.split(')')

    if obj not in space_objects:
        space_objects[obj] = SpaceObject(obj)
    if orbiting_object not in space_objects:
        space_objects[orbiting_object] = SpaceObject(orbiting_object)

    space_objects[obj].add_orbit(orbiting_object)

# for obj in space_objects:
#     print(f'{obj}: ', end='')
#     for orbiting in space_objects[obj].get_orbits():
#         print(f'{orbiting} ', end='')
#     print()

def count_orbits(space_objects, obj):
    orbits = space_objects[obj].get_orbits()
    if len(orbits) == 0:
        return 0, 1

    sum = 0
    count = 1
    for o in orbits:
        s, l = count_orbits(space_objects, o)
        count += l
        sum += s + l
    return sum, count

print(count_orbits(space_objects, 'COM'))
