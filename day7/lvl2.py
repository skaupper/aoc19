class IntComputer(object):
    def __init__(self, ram, io_functor, i=0):
        self.initial_ram = ram[:]
        self.set_io(io_functor)
        self.reset()
        self.i = i

    def reset(self):
        self.ram = self.initial_ram[:]
        self.ip = 0
        self.running = False

    def set_io(self, io_functor):
        self.io = io_functor

    def __getParam(self, idx, modes):
        val = self.ram[self.ip+idx+1]

        if len(modes) <= idx:
            mode = 0
        else:
            mode = modes[idx]

        if mode == 0:   # position mode
            return self.ram[val]
        elif mode == 1: # immediate mode
            return val
        raise KeyError("Unknown parameter mode detected")

    def stop(self):
        self.running = False

    def start(self):
        self.running = True

        while self.running:
            op = int(str(self.ram[self.ip])[-2:])
            modes = [int(s) for s in reversed(str(self.ram[self.ip])[:-2])]

            # Addition
            if op == 1:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                dest = self.ram[self.ip+3]
                self.ram[dest] = p1+p2
                self.ip += 4
            # Multiplication
            elif op == 2:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                dest = self.ram[self.ip+3]
                self.ram[dest] = p1*p2
                self.ip += 4
            # Input
            elif op == 3:
                dest = self.ram[self.ip+1]
                val = self.io.input()
                self.ram[dest] = val
                self.ip += 2
            # Output
            elif op == 4:
                src = self.__getParam(0, modes)
                self.io.output(src)
                self.ip += 2
            # Jump if not zero
            elif op == 5:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                if p1 != 0:
                    self.ip = p2
                else:
                    self.ip += 3
            # Jump if zero
            elif op == 6:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                if p1 == 0:
                    self.ip = p2
                else:
                    self.ip += 3
            # Less than
            elif op == 7:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                dest = self.ram[self.ip+3]
                if p1 < p2:
                    self.ram[dest] = 1
                else:
                    self.ram[dest] = 0
                self.ip += 4
            # Equal
            elif op == 8:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                dest = self.ram[self.ip+3]
                if p1 == p2:
                    self.ram[dest] = 1
                else:
                    self.ram[dest] = 0
                self.ip += 4
            # Halt
            elif op == 99:
                self.running = False
            else:
                raise KeyError('Unknown operation detected')

class IOQueueFunctor(object):
    def __init__(self, in_queue, out_queue):
        self.in_queue = in_queue
        self.out_queue = out_queue

    def input(self):
        return self.in_queue.get()

    def output(self, value):
        self.out_queue.put(value)


input_filename = 'input.txt'
# input_filename = 'test6.txt'
output_filename = 'output.txt'

rom = []
with open(input_filename, 'r') as f:
   rom = [int(s) for s in f.read().strip(' \n').split(',')]




import itertools
import queue
from threading import Thread



AMPLIFIERS = 5


permutations = list(itertools.permutations(range(5, 10)))
queues = [queue.SimpleQueue() for i in range(AMPLIFIERS+1)]
computers = []

for i in range(AMPLIFIERS):
    computers.append(IntComputer(rom, IOQueueFunctor(queues[i], queues[(i+1)%AMPLIFIERS]), i))

max_seq = None
max_boost = 0

for p in permutations:
    current_value = 0

    # prefill input queues
    for i in range(AMPLIFIERS):
        computers[i].reset()
        queues[i].put(p[i])
    queues[0].put(0)

    threads = []
    for i in range(AMPLIFIERS):
        t = Thread(target=computers[i].start)
        t.start()
        threads.append(t)

    for t in threads:
        t.join()

    current_value = queues[0].get()
    # print(current_value)

    if current_value > max_boost or max_seq is None:
        max_seq = p
        max_boost = current_value

print(max_seq)
print(max_boost)
