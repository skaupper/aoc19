class IntComputer(object):
    def __init__(self, ram, in_fn, out_fn):
        self.initial_ram = ram[:]
        self.set_io(in_fn, out_fn)
        self.reset()

    def reset(self):
        self.ram = self.initial_ram[:]
        self.ip = 0
        self.running = False

    def set_io(self, in_fn, out_fn):
        self.input = in_fn
        self.output = out_fn

    def __getParam(self, idx, modes):
        val = self.ram[self.ip+idx+1]

        if len(modes) <= idx:
            mode = 0
        else:
            mode = modes[idx]

        if mode == 0:   # position mode
            return self.ram[val]
        elif mode == 1: # immediate mode
            return val
        raise KeyError("Unknown parameter mode detected")

    def stop(self):
        self.running = False

    def start(self):
        self.running = True

        while self.running:
            op = int(str(self.ram[self.ip])[-2:])
            modes = [int(s) for s in reversed(str(self.ram[self.ip])[:-2])]

            # Addition
            if op == 1:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                dest = self.ram[self.ip+3]
                self.ram[dest] = p1+p2
                self.ip += 4
            # Multiplication
            elif op == 2:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                dest = self.ram[self.ip+3]
                self.ram[dest] = p1*p2
                self.ip += 4
            # Input
            elif op == 3:
                dest = self.ram[self.ip+1]
                val = self.input()
                self.ram[dest] = val
                self.ip += 2
            # Output
            elif op == 4:
                src = self.__getParam(0, modes)
                self.output(src)
                self.ip += 2
            # Jump if not zero
            elif op == 5:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                if p1 != 0:
                    self.ip = p2
                else:
                    self.ip += 3
            # Jump if zero
            elif op == 6:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                if p1 == 0:
                    self.ip = p2
                else:
                    self.ip += 3
            # Less than
            elif op == 7:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                dest = self.ram[self.ip+3]
                if p1 < p2:
                    self.ram[dest] = 1
                else:
                    self.ram[dest] = 0
                self.ip += 4
            # Equal
            elif op == 8:
                p1 = self.__getParam(0, modes)
                p2 = self.__getParam(1, modes)
                dest = self.ram[self.ip+3]
                if p1 == p2:
                    self.ram[dest] = 1
                else:
                    self.ram[dest] = 0
                self.ip += 4
            # Halt
            elif op == 99:
                self.running = False
            else:
                raise KeyError('Unknown operation detected')

def default_input():
    return int(input())

def default_output(value):
    print(value)


input_filename = 'input.txt'
# input_filename = 'test4.txt'
output_filename = 'output.txt'

rom = []
with open(input_filename, 'r') as f:
   rom = [int(s) for s in f.read().strip(' \n').split(',')]

comp = IntComputer(rom, default_input, default_output)






import itertools
import queue





permutations = list(itertools.permutations(range(0, 5)))
print(len(permutations))

last = queue.SimpleQueue()
next = queue.SimpleQueue()

def get_from_queue():
    if last.empty():
        raise ValueError('No value in queue!')
    return last.get()

def put_into_queue(value):
    next.put(value)

comp.set_io(get_from_queue, put_into_queue)

max_seq = None
max_boost = 0

computers = []
for i in range(5):
    c = IntComputer(rom, get_from_queue, put_into_queue)

for p in permutations:
    current_value = 0

    for phase in p:
        last.put(phase)
        last.put(current_value)
        comp.reset()
        comp.start()
        current_value = next.get()

    if current_value > max_boost or max_seq is None:
        max_seq = p
        max_boost = current_value

print(max_boost)
