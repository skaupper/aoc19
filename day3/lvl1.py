import numpy as np

input_file = 'input.txt'
# input_file = 'test2.txt'

line1, line2 = ([], [])
with open(input_file, 'r') as f:
    line1, line2 = [l.split(',') for l in f.read().strip(' \n').split('\n')]

# simulate lines to get the map dimensions
minx = 0
maxx = 0
miny = 0
maxy = 0
first = True
for l in [line1, line2]:
    posX = 0
    posY = 0

    for dir in l:
        dirX = 0
        dirY = 0

        if dir[0] == 'R':
            dirX = 1
        elif dir[0] == 'L':
            dirX = -1
        elif dir[0] == 'U':
            dirY = 1
        elif dir[0] == 'D':
            dirY = -1

        d = int(dir[1:])
        posX += d * dirX
        posY += d * dirY

        # calculate map dimenstions
        minx = min(posX, minx)
        maxx = max(posX, maxx)
        miny = min(posY, miny)
        maxy = max(posY, maxy)

print(minx)
print(maxx)
print(miny)
print(maxy)

map = np.full((maxy-miny+1, maxx-minx+1), '.')
cx = abs(minx)
cy = abs(miny)

print(cx, cy)

map[cy][cx] = 'o'

intersections = []
for i, l in enumerate([line1, line2]):
    posX = cx
    posY = cy

    for dir in l:
        dirX = 0
        dirY = 0

        if dir[0] == 'R':
            dirX = 1
        elif dir[0] == 'L':
            dirX = -1
        elif dir[0] == 'U':
            dirY = 1
        elif dir[0] == 'D':
            dirY = -1

        d = int(dir[1:])

        if dirX != 0:
            for x in range(posX, posX+(d+1)*dirX, dirX):
                if x == posX:
                    continue
                if map[posY][x] != '.' and map[posY][x] != str(i):
                    intersections.append((x, posY))
                map[posY][x] = str(i)
        elif dirY != 0:
            for y in range(posY, posY+(d+1)*dirY, dirY):
                if y == posY:
                    continue
                if map[y][posX] != '.' and map[y][posX] != str(i):
                    intersections.append((posX, y))
                map[y][posX] = str(i)

        posX += d*dirX
        posY += d*dirY


# for y in range(map.shape[0]-1, -1, -1):
#     for x in range(0, map.shape[1]):
#         print(f'{map[y][x]} ', end='')
#     print()


mind = 0
for i in intersections:
    if mind == 0:
        mind = abs(i[0]-cx)+abs(i[1]-cy)
    else:
        mind = min(mind, abs(i[0]-cx)+abs(i[1]-cy))

print(mind)
