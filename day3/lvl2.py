import numpy as np

input_file = 'input.txt'
# input_file = 'test2.txt'

line1, line2 = ([], [])
with open(input_file, 'r') as f:
    line1, line2 = [l.split(',') for l in f.read().strip(' \n').split('\n')]

# simulate lines to get the map dimensions
minx = 0
maxx = 0
miny = 0
maxy = 0
first = True
for l in [line1, line2]:
    posX = 0
    posY = 0

    for dir in l:
        dirX = 0
        dirY = 0

        if dir[0] == 'R':
            dirX = 1
        elif dir[0] == 'L':
            dirX = -1
        elif dir[0] == 'U':
            dirY = 1
        elif dir[0] == 'D':
            dirY = -1

        d = int(dir[1:])
        posX += d * dirX
        posY += d * dirY

        # calculate map dimenstions
        minx = min(posX, minx)
        maxx = max(posX, maxx)
        miny = min(posY, miny)
        maxy = max(posY, maxy)

print(minx)
print(maxx)
print(miny)
print(maxy)

map = np.empty((maxy-miny+1, maxx-minx+1), dtype=object)
cx = abs(minx)
cy = abs(miny)

print(cx, cy)
map[cy][cx] = 'o'

intersections = []
for i, l in enumerate([line1, line2]):
    posX = cx
    posY = cy
    c = 0

    for dir in l:
        dirX = 0
        dirY = 0

        if dir[0] == 'R':
            dirX = 1
        elif dir[0] == 'L':
            dirX = -1
        elif dir[0] == 'U':
            dirY = 1
        elif dir[0] == 'D':
            dirY = -1

        d = int(dir[1:])

        if dirX != 0:
            for x in range(posX, posX+(d+1)*dirX, dirX):
                if x == posX:
                    continue
                c += 1
                if map[posY][x] is not None and map[posY][x][0] != i:
                    intersections.append(map[posY][x][1]+c)
                map[posY][x] = (i, c)
        elif dirY != 0:
            for y in range(posY, posY+(d+1)*dirY, dirY):
                if y == posY:
                    continue
                c += 1
                if map[y][posX] is not None and map[y][posX][0] != i:
                    intersections.append(map[y][posX][1]+c)
                map[y][posX] = (i, c)

        posX += d*dirX
        posY += d*dirY


# for y in range(map.shape[0]-1, -1, -1):
#     for x in range(0, map.shape[1]):
#         print(f'{map[y][x]} ', end='')
#     print()


print(intersections)
print(min(intersections))
