lowerBound = 272091
upperBound = 815432

validPasswords = 0

for pw in range(lowerBound, upperBound+1):
    doubleDigitFound = False
    decreaseFound = False
    lastDigit = 0

    for d in str(pw):
        if int(d) < lastDigit:
            decreaseFound = True
            break
        if int(d) == lastDigit:
            doubleDigitFound = True
        lastDigit = int(d)

    if doubleDigitFound and not decreaseFound:
        validPasswords += 1

print(validPasswords)
