lowerBound = 272091
upperBound = 815432

validPasswords = 0

def validPassword(pw):
    doubleDigitFound = False
    count = 1
    decreaseFound = False
    lastDigit = 0

    for d in str(pw):
        if int(d) < lastDigit:
            decreaseFound = True
            break
        if int(d) == lastDigit:
            count += 1
        else:
            if count == 2:
                doubleDigitFound = True
            count = 1
        lastDigit = int(d)

    if count == 2:
        doubleDigitFound = True

    if doubleDigitFound and not decreaseFound:
        return True
    return False

print(validPassword(112233)) # True
print(validPassword(123444)) # False
print(validPassword(111122)) # True

for pw in range(lowerBound, upperBound+1):
    if validPassword(pw):
        validPasswords += 1

print(validPasswords)
