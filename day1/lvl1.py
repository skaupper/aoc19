import numpy as np

input_file = 'lvl1-input.txt'
# input_file = 'lvl1-test.txt'
output_file = 'lvl1-output.txt'

lines = []
with open(input_file, 'r') as f:
    lines = f.read().strip(' \n').split('\n')

fuel = []
for l in lines:
    val = int(int(l) / 3) - 2
    # print(f'{l} -> {val}')
    fuel.append(val)

total_fuel = int(np.sum(fuel))
print(total_fuel)

with open(output_file, 'w') as f:
    f.write(str(total_fuel))
