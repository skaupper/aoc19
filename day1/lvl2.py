import numpy as np

input_file = 'lvl2-input.txt'
# input_file = 'lvl2-test.txt'
output_file = 'lvl2-output.txt'

lines = []
with open(input_file, 'r') as f:
    lines = f.read().strip(' \n').split('\n')

fuel = []
for l in lines:
    add_fuel = int(l)
    module_fuel = -add_fuel
    while add_fuel > 0:
        module_fuel += add_fuel
        add_fuel = add_fuel // 3 - 2

    # print(f'{l} -> {module_fuel}')
    fuel.append(module_fuel)

total_fuel = int(np.sum(fuel))
print(total_fuel)

with open(output_file, 'w') as f:
    f.write(str(total_fuel))
