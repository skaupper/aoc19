from enum import IntEnum
import numpy as np
from math import ceil


input_filename = 'input.txt'
# input_filename = 'test.txt'
# input_filename = 'test2.txt'
# input_filename = 'test3.txt'


lines = []
with open(input_filename, 'r') as f:
    lines = f.read().strip(' \n').split('\n')

ingredients = []
recipes = {}

# determine ingredients
for l in lines:
    recipe = {}

    ings, res = l.split('=>')

    for ing in ings.strip().split(','):
        amt, name = ing.strip(', ').split(' ')
        recipe[name] = int(amt)

        if name not in ingredients:
            ingredients.append(name)

    amt, name = res.strip().split(' ')
    if name not in ingredients:
        ingredients.append(name)

    recipes[name] = {
        'amt': int(amt),
        'r': recipe
    }

# print(ingredients)
# print(recipes)

#
# Step 0: create index map
#
indices = {}
for i in range(len(ingredients)):
    indices[ingredients[i]] = i

#
# Step 1: remove dead ends (except FUEL)
#
dead = [True] * len(ingredients)
needed = {'FUEL': 1}

while len(recipes) > 0:
    dead = [True] * len(ingredients)
    for k in recipes:
        r = recipes[k]
        for ingredient in r['r']:
            dead[indices[ingredient]] = False

    for i in range(len(dead)):
        if ingredients[i] not in recipes:
            dead[i] = False

    # get dead end
    i = dead.index(True)
    curr = ingredients[i]

    # which recipe do we process
    # print(curr)
    r = recipes[curr]

    # how many times do we need the recipe?
    multiples = ceil(needed[curr] / r['amt'])

    # update needed ingredients
    for ing in r['r']:
        if ing not in needed:
            needed[ing] = 0
        needed[ing] += multiples * r['r'][ing]

    # delete processed ingredient from all maps
    del needed[curr]
    del recipes[curr]

    print(needed)
