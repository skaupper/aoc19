import copy

input_filename = 'lvl1-input.txt'
# input_filename = 'lvl1-test.txt'
output_filename = 'lvl1-output.txt'

rom = []
with open(input_filename, 'r') as f:
   rom = [int(s) for s in f.read().strip(' \n').split(',')]

target = 19690720

ram = []
for i in range(0,100000):
   noun = i // 100
   verb = i % 100

   # initialize RAM
   ram = copy.deepcopy(rom)
   ram[1] = noun
   ram[2] = verb

   ip = 0
   op = ram[ip]
   while True:
      print(f'op: {op}')
      if ram[ip] == 1:
         op1 = ram[ip+1]
         op2 = ram[ip+2]
         dest = ram[ip+3]
         ram[dest] = ram[op1]+ram[op2]
         ip += 4
      elif ram[ip] == 2:
         op1 = ram[ip+1]
         op2 = ram[ip+2]
         dest = ram[ip+3]
         ram[dest] = ram[op1]*ram[op2]
         ip += 4
      elif ram[ip] == 99:
         break
      else:
         print('wtf?')
         break
   
   if ram[0] == target:
      break

print(noun)
print(verb)
print(ram[0])