input_filename = 'input.txt'
# input_filename = 'test2.txt'
output_filename = 'output.txt'

pixels = []
with open(input_filename, 'r') as f:
   pixels = [int(s) for s in f.read().strip(' \n')]

WIDTH = 25
HEIGHT = 6
# WIDTH = 2
# HEIGHT = 2

print(f'Pixel count: {len(pixels)}')
print(f'Image dimensions: {WIDTH}x{HEIGHT}')

layer_count = int(len(pixels) / (WIDTH * HEIGHT))
layers = []

for i in range(layer_count):
    layers.append(pixels[i*WIDTH*HEIGHT:(i+1)*WIDTH*HEIGHT])


image = [2] * (WIDTH*HEIGHT)

for l in layers:
    for i in range(WIDTH*HEIGHT):
        if image[i] == 2:
            image[i] = l[i]

for y in range(HEIGHT):
    for x in range(WIDTH):
        v = image[x+y*WIDTH]
        if v == 0:
            print(' ', end='')
        else:
            print('x', end='')
    print()

# print(''.join([str(s) for s in image]))
