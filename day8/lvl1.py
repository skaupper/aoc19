input_filename = 'input.txt'
# input_filename = 'test.txt'
output_filename = 'output.txt'

pixels = []
with open(input_filename, 'r') as f:
   pixels = [int(s) for s in f.read().strip(' \n')]

WIDTH = 25
HEIGHT = 6

print(f'Pixel count: {len(pixels)}')
print(f'Image dimensions: {WIDTH}x{HEIGHT}')

layer_count = int(len(pixels) / (WIDTH * HEIGHT))
layers = []

for i in range(layer_count):
    layers.append(pixels[i*WIDTH*HEIGHT:(i+1)*WIDTH*HEIGHT])

print(layers)

layers.sort(key=lambda a: a.count(0))
print(layers[0].count(1)*layers[0].count(2))
