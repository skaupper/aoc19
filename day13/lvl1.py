from int_comp import IntComputer, IOQueueFunctor
from enum import IntEnum
from queue import Queue as SimpleQueue
from threading import Thread


input_filename = 'input.txt'
# input_filename = 'test.txt'
# input_filename = 'test2.txt'


class Vector2D(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'<x: {self.x:>4}, y: {self.y:>4}>'

    def __str__(self):
        return self.__repr__()

    def add(self, vec):
        self.x += vec.x
        self.y += vec.y


class TileId(IntEnum):
    EMPTY = 0
    WALL = 1
    BLOCK = 2
    HPADDLE = 3
    BALL = 4

    def to_char(self):
        if self == self.EMPTY:
            return ' '
        elif self == self.WALL:
            return '#'
        elif self == self.BLOCK:
            return '.'
        elif self == self.HPADDLE:
            return '_'
        elif self == self.BALL:
            return 'o'



class Tile(object):
    def __init__(self, pos, id):
        self.pos = pos
        self.id = id


rom = []
with open(input_filename, 'r') as f:
   rom = [int(s) for s in f.read().strip(' \n').split(',')]


in_queue = SimpleQueue()
out_queue = SimpleQueue()

pc = IntComputer(rom, IOQueueFunctor(in_queue, out_queue))
stopped = False

def comp_thread():
    global stopped
    stopped = False
    pc.start()
    stopped = True

t = Thread(target=comp_thread)
t.start()



map = {}

def get_tile(x, y):
    if not x in map:
        map[x] = {}
    if not y in map[x]:
        map[x][y] = Tile(Vector2D(x, y), TileId.EMPTY)
    return map[x][y]

def set_tile(x, y, tile):
    if not x in map:
        map[x] = {}
    if not y in map[x]:
        map[x][y] = Tile(Vector2D(x, y), TileId.EMPTY)
    map[x][y] = tile

def print_map(minx, maxx, miny, maxy):
    for y in range(miny, maxy+1):
        for x in range(minx, maxx+1):
            if x not in map or y not in map[x]:
                print(TileId.EMPTY.to_char(), end='')
            else:
                print(map[x][y].id.to_char(), end='')
        print()


first = True
min = Vector2D(0, 0)
max = Vector2D(0, 0)

while True:
    try:
        x = int(out_queue.get(timeout=1))
        y = int(out_queue.get(timeout=1))
        id = TileId(int(out_queue.get(timeout=1)))
    except:
        break

    tile = Tile(Vector2D(x, y), id)
    set_tile(x, y, tile)


    # set extremes
    if first:
        min.x = x
        min.y = y
        max.x = x
        max.y = y
        first = False

    if x < min.x:
        min.x = x
    if x > max.x:
        max.x = x
    if y < min.y:
        min.y = y
    if y > max.y:
        max.y = y


print_map(min.x, max.x, min.y, max.y)

sum = 0
for x in map:
    for y in map[x]:
        if map[x][y].id == TileId.BLOCK:
            sum += 1
print(sum)
