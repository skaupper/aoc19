import queue


class IOStdFunctor(object):
    def input(self):
        return int(input())

    def output(self, value):
        print(value)

class IOQueueFunctor(object):
    def __init__(self, in_queue, out_queue):
        self.in_queue = in_queue
        self.out_queue = out_queue

    def input(self):
        return self.in_queue.get()

    def output(self, value):
        self.out_queue.put(value)

class IntComputer(object):
    def __init__(self, rom, io_functor = IOStdFunctor()):
        self.flash(rom)
        self.set_io(io_functor)
        self.reset()

    def flash(self, rom):
        self.rom = rom[:]

    def reset(self):
        self.ram = self.rom[:]
        self.ip = 0
        self.running = False
        self.rel_base = 0

    def set_io(self, io_functor):
        self.io = io_functor

    def set_ram_size(self, size):
        if size >= len(self.ram):
            self.ram += [0] * (size - len(self.ram))

    def __resize_ram_on_access(self, idx):
        if idx >= len(self.ram):
            # print(f'Resize ram to {idx} entries')
            self.ram += [0] * (idx - len(self.ram) + 1)

    def __get_param(self, idx, modes):
        ram_idx = self.ip+idx+1
        self.__resize_ram_on_access(ram_idx)
        val = self.ram[ram_idx]

        if len(modes) <= idx:
            mode = 0
        else:
            mode = modes[idx]

        if mode == 0:   # position mode
            self.__resize_ram_on_access(val)
            return self.ram[val]
        elif mode == 1: # immediate mode
            return val
        elif mode == 2: # relative mode
            self.__resize_ram_on_access(self.rel_base+val)
            return self.ram[self.rel_base+val]
        raise KeyError('Unknown parameter mode detected')

    def __get_dest(self, idx, modes):
        ram_idx = self.ip+idx+1
        self.__resize_ram_on_access(ram_idx)
        val = self.ram[ram_idx]

        if len(modes) <= idx:
            mode = 0
        else:
            mode = modes[idx]

        if mode == 0:   # position mode
            self.__resize_ram_on_access(val)
            return val
        elif mode == 1: # immediate mode
            raise KeyError('Immediate mode not available for destinations')
        elif mode == 2: # relative mode
            self.__resize_ram_on_access(self.rel_base+val)
            return self.rel_base+val
        raise KeyError('Unknown parameter mode detected')


    def stop(self):
        self.running = False

    def start(self):
        self.running = True

        while self.running:
            op = int(str(self.ram[self.ip])[-2:])
            modes = [int(s) for s in reversed(str(self.ram[self.ip])[:-2])]

            # Addition
            if op == 1:
                p1 = self.__get_param(0, modes)
                p2 = self.__get_param(1, modes)
                dest = self.__get_dest(2, modes)
                self.ram[dest] = p1+p2
                self.ip += 4
            # Multiplication
            elif op == 2:
                p1 = self.__get_param(0, modes)
                p2 = self.__get_param(1, modes)
                dest = self.__get_dest(2, modes)
                self.ram[dest] = p1*p2
                self.ip += 4
            # Input
            elif op == 3:
                dest = self.__get_dest(0, modes)
                val = self.io.input()
                self.ram[dest] = val
                self.ip += 2
            # Output
            elif op == 4:
                src = self.__get_param(0, modes)
                self.io.output(src)
                self.ip += 2
            # Jump if not zero
            elif op == 5:
                p1 = self.__get_param(0, modes)
                p2 = self.__get_param(1, modes)
                if p1 != 0:
                    self.ip = p2
                else:
                    self.ip += 3
            # Jump if zero
            elif op == 6:
                p1 = self.__get_param(0, modes)
                p2 = self.__get_param(1, modes)
                if p1 == 0:
                    self.ip = p2
                else:
                    self.ip += 3
            # Less than
            elif op == 7:
                p1 = self.__get_param(0, modes)
                p2 = self.__get_param(1, modes)
                dest = self.__get_dest(2, modes)
                if p1 < p2:
                    self.ram[dest] = 1
                else:
                    self.ram[dest] = 0
                self.ip += 4
            # Equal
            elif op == 8:
                p1 = self.__get_param(0, modes)
                p2 = self.__get_param(1, modes)
                dest = self.__get_dest(2, modes)
                if p1 == p2:
                    self.ram[dest] = 1
                else:
                    self.ram[dest] = 0
                self.ip += 4
            elif op == 9:
                p1 = self.__get_param(0, modes)
                self.rel_base += p1
                self.ip += 2
            # Halt
            elif op == 99:
                self.running = False
            else:
                raise KeyError(f'Unknown operation detected: {op}')
